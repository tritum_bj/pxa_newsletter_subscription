<?php
if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	$_EXTKEY,
	'Subscription',
	array(
		'NewsLetterSubscription' => 'form,ajax',
	),
	// non-cacheable actions
	array(
		'NewsLetterSubscription' => 'ajax',
	)
);
?>
