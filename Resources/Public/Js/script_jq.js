jQuery(document).ready(function() {

	jQuery('#contact-form').on('submit', function(e) {
		e.preventDefault();

		var subscriptionStatus = 1;
		var dataFromForm = jQuery('#contact-form').serializeArray();
		// unique tag id
		var ts = Math.round(+new Date());
		var messageTagId = 'contact-form-message' + ts;
		
		jQuery('#contact-form button[type="submit"] i').fadeIn(50);
		jQuery('#contact-form button[type="submit"]').attr('disabled', 'disabled');
		
		jQuery.ajax({
			//type of receiving data
			type: 'POST',
			//page where ajax is running
			url: jQuery('#contact-form').attr('action') + '?type=6171240&tx_pxanewslettersubscription_subscription%5Baction%5D=ajax',

			//send_data, data which will be send to php
			data: dataFromForm,
			dataType: "JSON",
			//msg - data which are returned from php
			success: function(response) {
				//ajax sends msg from php, which informs user, what has happens
				jQuery('#contact-form').after('<div id="' + messageTagId + '" class="alert">' + response.message + '<div>');
				if (response.success) {
					jQuery('#' + messageTagId).addClass('alert-success');
					jQuery('#contact-form').hide();
				} else {
					jQuery('#' + messageTagId).addClass('alert-danger');
					// Set message to disapear after 5 sec.
					jQuery('#' + messageTagId).delay(5000).fadeOut('slow');
				}
				// Hide spinner and enable button
				jQuery('#contact-form button[type="submit"] i').fadeOut(50);
				jQuery('#contact-form button[type="submit"]').removeAttr('disabled');
			},

			error: function(jqXHR, textStatus, errorThrown) {
				// Set message and set it to disapear after 5 sec.
				jQuery('#contact-form').after('<div id="' + messageTagId + '" class="alert alert-danger">' + jqXHR_error_message + '<div>');
				jQuery('#' + messageTagId).delay(5000).fadeOut('slow');
				// Hide spinner and enable button
				jQuery('#contact-form button[type="submit"] i').fadeOut(50);
				jQuery('#contact-form button[type="submit"]').removeAttr('disabled');
			}
		}); //end ajax

	}); //end submit

});