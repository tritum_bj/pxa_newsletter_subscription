$(document).ready(function() {

	$(document).acknowledgeinput({
		default_state: 'hidden'
	});

	$('#contact-form').on('submit', function(e) {
		e.preventDefault();

		var subscriptionStatus = 1;
		var dataFromForm = $('#contact-form').serializeArray();
		// unique tag id
		var ts = Math.round(+new Date());
		var messageTagId = 'contact-form-message' + ts;
		
		$('#contact-form button[type="submit"] i').fadeIn(50);
		$('#contact-form button[type="submit"]').attr('disabled', 'disabled');
		
		$.ajax({
			//type of receiving data
			type: 'POST',
			//page where ajax is running
			url: $('#contact-form').attr('action') + '?type=6171240&tx_pxanewslettersubscription_subscription%5Baction%5D=ajax',

			//send_data, data which will be send to php
			data: dataFromForm,
			dataType: "JSON",
			//msg - data which are returned from php
			success: function(response) {
				//ajax sends msg from php, which informs user, what has happens
				$('#contact-form').after('<div id="' + messageTagId + '" class="alert">' + response.message + '<div>');
				if (response.success) {
					$('#' + messageTagId).addClass('alert-success');
					$('#contact-form').hide();
				} else {
					$('#' + messageTagId).addClass('alert-danger');
					// Set message to disapear after 5 sec.
					$('#' + messageTagId).delay(5000).fadeOut('slow');
				}
				// Hide spinner and enable button
				$('#contact-form button[type="submit"] i').fadeOut(50);
				$('#contact-form button[type="submit"]').removeAttr('disabled');
			},

			error: function(jqXHR, textStatus, errorThrown) {
				// Set message and set it to disapear after 5 sec.
				$('#contact-form').after('<div id="' + messageTagId + '" class="alert alert-danger">' + jqXHR_error_message + '<div>');
				$('#' + messageTagId).delay(5000).fadeOut('slow');
				// Hide spinner and enable button
				$('#contact-form button[type="submit"] i').fadeOut(50);
				$('#contact-form button[type="submit"]').removeAttr('disabled');

			}
			


		}); //end ajax
	}); //end submit

});
