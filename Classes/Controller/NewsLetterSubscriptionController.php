<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Inna Titorenko <inna@pixelant.se>, Pixelant
 *  (c) 2013 Jozef Spisiak <jozef@pixelant.se>, Pixelant
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Rendering the form
 *
 * @package pxa_newsletter_subscription
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */


use \TYPO3\CMS\Core\Utility\GeneralUtility as Utility, 
	\TYPO3\CMS\Extbase\Utility\LocalizationUtility as Localize;

/**
* Controller for newslettersubscription
*/
class Tx_PxaNewsletterSubscription_Controller_NewsLetterSubscriptionController extends Tx_Extbase_MVC_Controller_ActionController
{

	/**
	 * frontendUserRepository
	 *
	 * @var Pixelant\PxaNewsletterSubscription\Domain\Repository\FrontendUserRepository
	 * @inject
	 */
	protected $frontendUserRepository;

	/**
	 * frontendUserGroupRepository
	 *
	 * @var Pixelant\PxaNewsletterSubscription\Domain\Repository\FrontendUserGroupRepository
	 * @inject
	 */
	protected $frontendUserGroupRepository;

	/**
	 * persistence manager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
	 * @inject
	 */
	protected $persistenceManager;

	/**
	* Render form action
	*
	* @return void
	*/
	public function formAction()
	{
		$this->ceData = $this->configurationManager->getContentObject()->data;	
		$this->view->assign('ceuid',$this->ceData['uid']);
	}

	/**
	* Render ajax action
	*
	* @return void
	*/
	public function ajaxAction()
	{
		$hash = Utility::_GP('hash');
		
		if (!isset($hash)) {
			$response = $this->runAjax();
			header('Content-type: application/json');
			echo json_encode($response);
			exit;
		} else {
			$status = Utility::_GP('status');
			$hash = strip_tags(Utility::_GP('hash'));
			$id = intval(Utility::_GP('hashid'));
			
			if ($status == 'subscribe') {
				$this->confirmSubscription($hash,$id);
			} elseif ($status == 'unsubscribe') {
				$this->unsubscribe($hash,$id);
			}
		}
	}

	/**
	* runs ajax
	*
	* @return array
	*/
	public function runAjax()
	{
		$name = strip_tags(Utility::_GP('name'));
		$email = strip_tags(Utility::_GP('email'));
		$email = filter_var($email,FILTER_VALIDATE_EMAIL);

		$data = Utility::_GP('tx_pxanewslettersubscription_subscription');
		$pid = intval($this->settings['saveFolder']);
		$confirm = intval($this->settings['enableEmailConfirm']);
		$userGroup = intval($this->settings['userGroup']);

		if ($name == '' || !$email)
			return array(
				'message' => Localize::translate('invalid_name_or_email', 'pxa_newsletter_subscription'), 
				'success' => FALSE
				);

			// Check if existing current email in database
		$countEmail = $this->frontendUserRepository->getCountByEmailAndPid($email, $pid);

		if ($countEmail == 0) {
			
				// Fetch usergroup by uid from flexform settings
			$frontendUserGroup = $this->frontendUserGroupRepository->getFrontendUserGroupByUid($userGroup);

				// If no frontend user group was found, generate standard error and return.
			if ($frontendUserGroup === NULL) {
					// TODO: generate email for admin, setup invalid frontend usergroup is invalid.

				return array(
					'message' => Localize::translate('subscribe_error', 'pxa_newsletter_subscription') . " (3494)",
					'success' => FALSE
				);
			}

				// Create new Frontend User
			$frontendUser = new Pixelant\PxaNewsletterSubscription\Domain\Model\FrontendUser();

				// Set subscriber properties
			$frontendUser->setAsSubscriber($pid, $email, $name, intval($confirm), $frontendUserGroup);
				
				// Add Frontend User to repositoty
			$this->frontendUserRepository->add($frontendUser);

				// Save Frontend User
			$this->persistenceManager->persistAll();

				// If Frontend User was saved
			if ($frontendUser->getUid() > 0) {
				if ($confirm == 1) {
						// Send confirmation mail
					$this->sendConfirmationEmail($frontendUser->getEmail(), $frontendUser->getName(), $frontendUser->getHash(), $frontendUser->getUid());
				}
				$message = Localize::translate('subscribe_ok', 'pxa_newsletter_subscription');
				$success = TRUE;
			} else {
				$message = Localize::translate('subscribe_error', 'pxa_newsletter_subscription');
				$success = FALSE;
			}

		} else { 
				// if email have already exist in database, do not input anything, and show the appropriate message
			$message = Localize::translate('already_subscribed', 'pxa_newsletter_subscription');
			$success = FALSE;
		}

		return array(
			'message' => $message, 
			'success' => $success
		);
	}

	/**
	* Sends confirmation mail
	*
	* @param string $email
	* @param string $name
	* @param string $hash
	* @param string $id
	* @return void
	*/
	protected function sendConfirmationEmail($email, $name, $hash, $id)
	{
		$host = Utility::getIndpEnv('TYPO3_REQUEST_HOST');
		$domain = Utility::getIndpEnv('TYPO3_HOST_ONLY');
		if (substr($domain,0,3) == 'www')
		{
			$domain = substr($domain, 4);
			$sender = 'no-reply@' . $domain;
		} else {
			$sender = 'no-reply@' . $domain;
		}

			// Generate subscribe link
		$subscribeParams = array(
			"status" => "subscribe",
			"hashid" => $id,
			"hash" => $hash,
			"tx_pxanewslettersubscription_subscription" => array('action' => 'ajax'),
		);
		
		$subscribeLink = $this
			->uriBuilder
			->setTargetPageType(6171239)
			->setArguments($subscribeParams)
			->setUseCacheHash(false)
			->setCreateAbsoluteUri(true)
			->buildFrontendUri();

			// Generate unsubscribe link
		$unsubscribeParams = array(
			"status" => "unsubscribe",
			"hashid" => $id,
			"hash" => $hash,
			"tx_pxanewslettersubscription_subscription" => array('action' => 'ajax'),
		);
		
		$unsubscribeLink = $this
			->uriBuilder
			->setTargetPageType(6171239)
			->setArguments($unsubscribeParams)
			->setUseCacheHash(false)
			->setCreateAbsoluteUri(true)
			->buildFrontendUri();

		$subject = Localize::translate('confirm_mail_subject', 'pxa_newsletter_subscription');
		$message = Localize::translate('confirm_mail_greeting', 'pxa_newsletter_subscription',array($name)) . PHP_EOL . PHP_EOL . 
		Localize::translate('confirm_mail_line1', 'pxa_newsletter_subscription', array( PHP_EOL . PHP_EOL . $subscribeLink . PHP_EOL . PHP_EOL)) .
		Localize::translate('confirm_mail_line2', 'pxa_newsletter_subscription',array( PHP_EOL . PHP_EOL . $unsubscribeLink . PHP_EOL . PHP_EOL));

		$mail = Utility::makeInstance('t3lib_mail_Message');
		$mail->setFrom(array($sender => $sender));
		$mail->setTo(array($email => $email));
		$mail->setSubject($subject);
		$mail->setBody($message, 'text/plain');
		$mail->send();
	}

	/**
	* Confirms subscription
	*
	* @param string $hash
	* @param string $id
	* @return void
	*/
	protected function confirmSubscription($hash,$id) {

		try {	
		
				// Try to get the Frontenduser bu uid and hash
			$frontendUser = $this->frontendUserRepository->getUserByUidAndHash($id, $hash);
				
				// If a Frontenduser is found
			if ($frontendUser !== NULL) {
				
					// Set disable to false
				$frontendUser->setDisable(0);
				
					// Add Frontend User to repositoty
				$this->frontendUserRepository->update($frontendUser);

					// Save Frontend User
				$this->persistenceManager->persistAll();

					// Set returnmessage
				$message = Localize::translate('subscribe_ok', 'pxa_newsletter_subscription');
			}

		} catch (Exception $e) {
		
		}

		if (!isset($message))
		{
			$message = Localize::translate('subscribe_error', 'pxa_newsletter_subscription'); 
		}

		$this->view->assign('message', $message);
	}

	/**
	* Unsubscribe
	*
	* @param string $hash
	* @param string $id
	* @return void
	*/
	protected function unsubscribe($hash,$id) 
	{

		try {	
			
				// Try to get the Frontenduser bu uid and hash
			$frontendUser = $this->frontendUserRepository->getUserByUidAndHash($id, $hash);
				
				// If a Frontenduser is found
			if ($frontendUser !== NULL) {
				
					// Set deleted to true
				$frontendUser->setDeleted(1);
				
					// Add Frontend User to repositoty
				$this->frontendUserRepository->update($frontendUser);

					// Save Frontend User
				$this->persistenceManager->persistAll();

					// Set returnmessage
				$message = Localize::translate('unsubscribe_ok', 'pxa_newsletter_subscription');
			
			}

		} catch (Exception $e) {
		
		}

		if (!isset($message))
		{
			$message = Localize::translate('unsubscribe_error', 'pxa_newsletter_subscription'); 
		}


		$this->view->assign('message', $message);
	}

	/**
	* direct action
	*
	* @return array
	*/
	public function directAction()
	{
		return json_encode(
				array(
				'message' => 'test', 
				'success' => TRUE
				)
		);				
	}
}
?>