plugin.tx_pxanewslettersubscription {
	view {
		templateRootPath = {$plugin.tx_pxanewslettersubscription.view.templateRootPath}
		partialRootPath = {$plugin.tx_pxanewslettersubscription.view.partialRootPath}
		layoutRootPath = {$plugin.tx_pxanewslettersubscription.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_pxanewslettersubscription.persistence.storagePid}
	}
	features {
		# uncomment the following line to enable the new Property Mapper.
		# rewrittenPropertyMapper = 1
	}
}

config.tx_extbase.persistence.classes {
    Pixelant\PxaNewsletterSubscription\Domain\Model\FrontendUser {
        mapping {
            tableName = fe_users
        }
    }
    Pixelant\PxaNewsletterSubscription\Domain\Model\FrontendUserGroup {
        mapping {
            tableName = fe_groups
        }
    }
}

page.includeJSFooterlibs.bootstrapAcknowledgeInput = EXT:pxa_newsletter_subscription/Resources/Public/Js/bootstrap-acknowledgeinput.min.js
page.includeJSFooterlibs.pxa_newsletter_subscription = EXT:pxa_newsletter_subscription/Resources/Public/Js/script.js


#
# AJAX Page Prototype (cannot remove all headers and set type because of confirmation and unsubscribe links in mail displays html message)
#
lib.AJAXPrototype= PAGE
lib.AJAXPrototype {
	typeNum = 6171230
	config {
		#disableAllHeaderCode = 1
		#xhtml_cleaning = 0
		admPanel = 0
		debug = 0
		no_cache = 1
		#additionalHeaders = Content-type:application/json
	}
}

#
# Pagetype 
#
AJAX_CONFIRM < lib.AJAXPrototype
AJAX_CONFIRM {
	typeNum = 6171239
  	10 < tt_content.list.20.pxanewslettersubscription_subscription
}

AJAX_REGISTER < lib.AJAXPrototype
AJAX_REGISTER {
	typeNum = 6171240  	
  	10 = RECORDS
	10.tables = tt_content
	10.source.data = GP:tx_pxanewslettersubscription_subscription|ceuid
	10.source.intval = 1
}



